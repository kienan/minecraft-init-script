# Mnemosyne Init Script

A SysV init script for Minecraft

## License and Copyright

Based on [minecraft-init-script][2].

Distributed under the MIT License, see LICENSE for the exact terms.

## Installation

1. Clone or unpack a copy of the files into the folder /etc/minecraft
1. Create a symbolic link or copy the config file into /etc/default/minecraft

    ln --symbolic /etc/minecraft/config /etc/default/minecraft

1. Create a symbolic link or copy the init script into /etc/init.d/minecraft

    ln --symbolic /etc/minecraft/minecraft /etc/init.d/minecraft

1. Ensure /etc/init.d/minecraft (or the file it points to) is executable

    chmod +x /etc/minecraft/minecraft

1. Configure /etc/default/minecraft as needed
1. Run: update-rc.d minecraft defaults

## Configuration

Set the MNEMOSYNE\_DATA\_DIR to your data directory (which contains default.db
and config.db). The init script assumes a minecraft user exists and has a
home directory at /home/minecraft. The data directory defaults to
/home/minecraft/minecraft-data.

Change any settings necessary, the defaults are noted in comments in the config file.

The default run-levels:

* Start on: 2 3 4 5
* Stop on:  0 1 6

## Notes

The pid directory /var/run/minecraft and log directory /var/log/minecraft will
be created and chowned to the user set if they don't exist. If you change the
user, re-chown the directories and files in them.

[2]: https://gitlab.com/kienan/minecraft-init-script

