#!/bin/bash
#
# minecraft	     Startup script for a minecraft instance
#
### BEGIN INIT INFO
# Provides: mnemosyne
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Mnemosyne spaced-repitition flash-card tool synchronisation server
# Description:       Mnemosyne spaced-repitition flash-card tool synchronisation server
### END INIT INFO

# Load defaults if available.
test -f /etc/default/minecraft && . /etc/default/minecraft

if [ ! -d $MINECRAFT_SERVER_DIR ] ; then
    echo "Unable to start, data directory \"$MINECRAFT_SERVER_DIR\" does not exist or is not a directory."
    exit 1
fi

if [ -z $MINECRAFT_USER ] ; then
    user="minecraft"
else
    user=$MINECRAFT_USER
fi

if [ -z $MINECRAFT_SERVER_BIN ] ; then
    cmd="$MINECRAFT_SERVER_DIR/ServerStart.sh"
else
    cmd="$MINECRAFT_SERVER_DIR/$MINECRAFT_SERVER_BIN"
fi

if [ -s $MINECRAFT_EXTRA_ARGS ] ; then
    cmd="$cmd $MINECRAFT_EXTRA_ARGS"
fi

if [ -z $RCON_ADDRESS ] ; then
    RCON_ADDRESS=localhost
fi

if [ -z $RCON_PORT ] ; then
    RCON_PORT=25575
fi

name=`basename $0`
pid_file="/var/run/$name/$name.pid"
stdout_log="/var/log/$name/$name.log"
stderr_log="/var/log/$name/$name.err"

# Create and chown /var/run/$name if needed
if [ ! -d `dirname $pid_file` ] ; then
    mkdir `dirname $pid_file`
    chown $user:$user `dirname $pid_file`
fi

# Create and chown log-directory if needed
if [ ! -d `dirname $stdout_log` ] ; then
    mkdir `dirname $stdout_log`
    chown $user:$user `dirname $stdout_log`
fi

get_pid() {
    cat "$pid_file"
}

is_running() {
    [ -f "$pid_file" ] && ps `get_pid` > /dev/null 2>&1
}

rcon_cmd() {
    $RCON_BIN -c -H $RCON_ADDRESS -P $RCON_PORT -p $RCON_PASSWORD "$1"
}

case "$1" in
    start)
	if is_running; then
	    echo "Already started"
	else
	    echo "Starting $name"
	    cd "$dir"
	    if [ -z "$user" ]; then
		su -c "$cmd >> $stdout_log 2>> $stderr_log & echo \$! > $pid_file"
	    else
		su -c "$cmd >> $stdout_log 2>> $stderr_log & echo \$! > $pid_file" "$user"
	    fi
            pgrep -P `get_pid` > $pid_file
	    if ! is_running; then
		echo "Unable to start, see $stdout_log and $stderr_log"
		echo "Last 10 lines of $stderr_log :"
		tail $stderr_log
		exit 1
	    fi
	fi
	;;
    stop)
	if is_running; then
	    echo -n "Stopping $name.."
            if [ "$USE_RCON" == 'TRUE' ] ; then
                rcon_cmd "stop" # just assume this works?
                sleep 5
            else
                kill `get_pid`
	        for i in {1..15}
	        do
		    if ! is_running; then
		        break
		    fi
     		    echo -n "."
		    sleep 1
	        done
	        echo
            fi
	    if is_running; then
		echo "Not stopped; may still be shutting down or shutdown may have failed"
		exit 1
	    else
		echo "Stopped"
		if [ -f "$pid_file" ]; then
		    rm "$pid_file"
		fi
	    fi
	else
	    echo "Not running"
	fi
	;;
    restart)
	$0 stop
	if is_running; then
	    echo "Unable to stop, will not attempt to start"
	    exit 1
	fi
	$0 start
	;;
    status)
	if is_running; then
	    echo "Running"
	else
	    echo "Stopped"
	    exit 1
	fi
	;;
    *)
	echo "Usage: $0 {start|stop|restart|status}"
	exit 1
	;;
esac

exit 0

